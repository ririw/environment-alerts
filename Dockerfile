FROM rust:1.48 as builder
WORKDIR /usr/src/myapp
COPY Cargo.toml .
COPY Cargo.lock .
RUN mkdir src
RUN bash -c "echo 'fn main() {}' > src/main.rs"
RUN cargo build --release
COPY . .
RUN cargo install --path .

FROM debian:buster-slim
COPY --from=builder /usr/local/cargo/bin/environment-monitor /usr/local/bin/environment-monitor
RUN ls /usr/local/bin/environment-monitor
CMD ["environment-monitor"]
