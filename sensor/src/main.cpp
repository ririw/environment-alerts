#include <Arduino.h>
#include <WiFiClientSecure.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"
#include "HTTPClient.h"

#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

#include "secrets.hpp"

#define STRINGIFY(x) #x

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 3600, 60000);
Adafruit_BME680 bme;

void setup_net() {
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print("*");
  }

  Serial.println("Connected to network");
//   wifi_client.setCACert((const char*)keys_ca_crt);
}


void setup() {
    Serial.begin(9600);

    setup_net();
    timeClient.begin();
    timeClient.update();
    while (!bme.begin()) {
        Serial.println("Could not find sensor!");
        delay(1000);
    }

    bme.setTemperatureOversampling(BME680_OS_8X);
    bme.setHumidityOversampling(BME680_OS_2X);
    bme.setPressureOversampling(BME680_OS_4X);
    bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
}

void loop() {
    static StaticJsonDocument<200> doc;
    HTTPClient http;

    if (random(0, 1000) <= 1) {
        Serial.println("Updating timestamp");
        timeClient.update();
    }
    
    if (!bme.performReading()) {
        Serial.println("Failed to perform reading :(");
    } else {
        long long epoch = timeClient.getEpochTime();
       
        doc["value"] = bme.humidity;
        doc["measure"] = "humidity";
        doc["scope"] = "logger";
        doc["timestamp"] = epoch;

        String output;
        serializeJson(doc, output);

        Serial.println(output);
        
        http.begin("https://dev.richardweiss.net/measurement", root_ca);
        http.addHeader("Auth", auth_key);
        int httpResponseCode = http.PUT(output);
        if (httpResponseCode != 200) {
            Serial.print("Request failed: ");
            Serial.println(http.getString());
        }

        // TEMPERATURE 
        doc["value"] = bme.temperature;
        doc["measure"] = "temperature";
        doc["scope"] = "logger";
        doc["timestamp"] = epoch;

        String output;
        serializeJson(doc, output);

        Serial.println(output);
        
        http.begin("https://dev.richardweiss.net/measurement", root_ca);
        http.addHeader("Auth", auth_key);
        int httpResponseCode = http.PUT(output);
        if (httpResponseCode != 200) {
            Serial.print("Request failed: ");
            Serial.println(http.getString());
        }

    }
    delay(10000);
}

void send_humidity(long long epoch) {

}