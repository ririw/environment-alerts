use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Debug)]
pub enum StoreError {
    PostgresDatabaseError(postgres::Error),
}

impl std::convert::From<postgres::Error> for StoreError {
    fn from(err: postgres::Error) -> Self {
        return StoreError::PostgresDatabaseError(err);
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Measure {
    pub scope: String,
    pub value: f64,
    pub measure: String,
    pub timestamp: i64,
}

pub trait Store {
    fn commit(&mut self, measure: Measure) -> Result<(), StoreError>;
    fn items(&mut self) -> Result<Vec<Measure>, StoreError>;
}

pub struct PostgresStore {
    client: postgres::Client,
}

impl PostgresStore {
    pub fn new(mut client: postgres::Client) -> Result<PostgresStore, postgres::error::Error> {
        client.batch_execute(
            "
            CREATE TABLE IF NOT EXISTS measure (
                id        SERIAL PRIMARY KEY,
                scope     TEXT NOT NULL,
                value     DOUBLE PRECISION NOT NULL,
                measure   TEXT NOT NULL,
                timestamp TIMESTAMPTZ NOT NULL
            )
        ",
        )?;
        Ok(PostgresStore { client })
    }
}

impl Store for PostgresStore {
    fn commit(&mut self, measure: Measure) -> Result<(), StoreError> {
        let naive = chrono::NaiveDateTime::from_timestamp(measure.timestamp, 0);
        let datetime: DateTime<Utc> = DateTime::from_utc(naive, Utc);

        self.client.execute(
            "INSERT INTO measure (scope, value, measure, timestamp) VALUES ($1, $2, $3, $4)",
            &[&measure.scope, &measure.value, &measure.measure, &datetime],
        )?;

        Ok(())
    }

    fn items(&mut self) -> Result<Vec<Measure>, StoreError> {
        let mut result = Vec::new();
        for row in self
            .client
            .query("SELECT scope, value, measure, timestamp FROM measure", &[])?
        {
            let scope: &str = row.get(0);
            let value: f64 = row.get(1);
            let measure: &str = row.get(2);
            let timestamp: DateTime<Utc> = row.get(3);
            result.push(Measure {
                scope: scope.to_string(),
                value: value,
                measure: measure.to_string(),
                timestamp: timestamp.timestamp(),
            })
        }

        Ok(result)
    }
}

pub struct DummyStore {
    data: Vec<Measure>,
}

impl DummyStore {
    pub fn new() -> DummyStore {
        DummyStore { data: Vec::new() }
    }
}

impl Default for DummyStore {
    fn default() -> Self {
        Self::new()
    }
}

impl Store for DummyStore {
    fn commit(&mut self, measure: Measure) -> Result<(), StoreError> {
        self.data.push(measure);
        if self.data.len() > 10000 {
            self.data.remove(0);
        }
        Ok(())
    }

    fn items(&mut self) -> Result<Vec<Measure>, StoreError> {
        Ok(self.data.clone())
    }
}
