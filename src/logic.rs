use crate::store::Measure;
use ureq::json;

#[derive(Clone, Debug)]
pub struct PushService {
    pub token: String,
    pub user: String,
    pub pushed_dry_message: bool,
}

impl PushService {
    fn log_push(&self, msg: &str) {
        println!("Pushing message: {:?}", msg);
        let response = ureq::post("https://api.pushover.net/1/messages.json").send_json(json!({
            "token": self.token,
            "user": self.user,
            "message": msg
        }));

        let is_ok = response.ok();
        if !is_ok {
            let status = response.status();
            let content = response.into_string();
            println!("Error during send: {} {:?}", status, content);
        }
    }

    pub fn check_push(&mut self, measures: &[Measure]) {
        let mut inside_measure: f64 = -1.0;
        let mut outside_measure: f64 = -1.0;
        for i in (0..measures.len()).rev() {
            let measure = &measures[i];
            #[allow(clippy::float_cmp)]
            if measure.measure == "inside" && inside_measure == -1.0 {
                inside_measure = measure.value
            }
            #[allow(clippy::float_cmp)]
            if measure.measure == "outside" && outside_measure == -1.0 {
                outside_measure = measure.value
            }

            #[allow(clippy::float_cmp)]
            if outside_measure != -1.0 && inside_measure != -1.0 {
                break;
            }
        }
        #[allow(clippy::float_cmp)]
        if outside_measure == -1.0 || inside_measure == -1.0 {
            return;
        }

        let is_dry_outside = outside_measure < 50.0;
        let is_wet_outside = outside_measure > 65.0;
        let is_wetter_inside = inside_measure > outside_measure + 5.0;

        if self.pushed_dry_message {
            if is_wet_outside {
                self.log_push("High humidity - close the windows!");
                self.pushed_dry_message = false;
            }
        } else if is_dry_outside && is_wetter_inside {
            self.log_push("Low humidity - open the windows!");
            self.pushed_dry_message = true;
        }
    }
}
