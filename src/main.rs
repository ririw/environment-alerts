use anyhow::Context;
use environment_monitor::{logic::PushService, store};
use std::env;
use std::sync::{Arc, Mutex};
use warp::Reply;
use warp::{http::Response, Filter};

fn check_auth(key: &Option<String>, auth_key: String) -> Option<Response<String>> {
    if let Some(k) = key {
        if k != &auth_key {
            return Some(
                Response::builder()
                    .status(401)
                    .body("Unauthorised".to_owned())
                    .unwrap(),
            );
        }
    }
    None
}

fn handle_put(
    store: Arc<Mutex<dyn store::Store + Send>>,
    client: Arc<Mutex<PushService>>,
    key: &Option<String>,
    auth_key: String,
    msg: store::Measure,
) -> Response<String> {
    if let Some(a) = check_auth(key, auth_key) {
        return a;
    }
    let mut store_v = store.lock().unwrap();

    println!("{:?}", msg);
    client.lock().unwrap().check_push(&store_v.items().unwrap());
    match store_v.commit(msg) {
        Ok(_) => Response::builder()
            .status(200)
            .body("Ok".to_owned())
            .unwrap(),
        Err(e) => Response::builder()
            .status(500)
            .body(format!("Failure: {:?}", e))
            .unwrap(),
    }
}

fn handle_pull(
    store: Arc<Mutex<dyn store::Store + Send>>,
    key: &Option<String>,
    auth_key: String,
    scope: String,
) -> warp::reply::Response {
    if let Some(k) = key {
        if k != &auth_key {
            let res = warp::reply::json(&"error".to_owned()).into_response();
            return warp::reply::with_status(res, warp::http::StatusCode::UNAUTHORIZED)
                .into_response();
        }
    }

    let mut store_v = store.lock().unwrap();
    let all_items = store_v.items().unwrap();
    let scope_items: Vec<store::Measure> = all_items
        .iter()
        .filter(|a| a.scope == scope)
        .map(|a| a.to_owned())
        .collect();
    warp::reply::json(&scope_items).into_response()
}

//Example: {"scope":"large","inside":5.0,"outside":3.0,"timestamp":"2020-11-30T22:51:10.179003Z"}

#[tokio::main]
async fn main() {
    let addr = if cfg!(debug_assertions) {
        [127, 0, 0, 1]
    } else {
        [0, 0, 0, 0]
    };

    let port = match env::var("PORT") {
        Ok(p) => p.parse::<u16>().unwrap(),
        Err(..) => 8000,
    };

    let conn_str = env::var("DOKKU_POSTGRES_AQUA_URL")
        .context("Need DOKKU_POSTGRES_AQUA_URL to connect")
        .unwrap();
    let client = postgres::Client::connect(&conn_str, postgres::NoTls)
        .context("Connecting to DB")
        .unwrap();
    let store_inst: Arc<Mutex<dyn store::Store + Send>> =
        Arc::new(Mutex::new(store::PostgresStore::new(client).unwrap()));
    let token = env::var("PUSH_TOKEN")
        .context("Attempted to read PUSH_TOKEN from env failed")
        .unwrap();
    let user = env::var("PUSH_USER")
        .context("Attempted to read PUSH_USER from env failed")
        .unwrap();
    let key: Option<String> = env::var("KEY").map(Some).unwrap_or(None);
    let client_inst = Arc::new(Mutex::new(PushService {
        token,
        user,
        pushed_dry_message: false,
    }));

    println!("Serving on: {:?}:{}", addr, port);
    let ping = warp::path!("ping" / String).map(|name| format!("pong - {}", name));

    let push_inst = Arc::clone(&store_inst);
    let pushkey = key.clone();
    let push = warp::path!("measurement")
        .and(warp::put())
        .and(warp::header("auth"))
        .and(warp::body::json())
        .map(move |k, a| {
            handle_put(
                Arc::clone(&push_inst),
                Arc::clone(&client_inst),
                &pushkey,
                k,
                a,
            )
        });
    let show = warp::path!("measurement" / String)
        .and(warp::get())
        .and(warp::header("auth"))
        .map(move |scope, k| handle_pull(Arc::clone(&store_inst), &key, k, scope));

    warp::serve(ping.or(push).or(show)).run((addr, port)).await;
}
